//
//  Colors.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit

struct Colors {
    static let matteGold = UIColor(red: 153/255.0, green: 118/255.0, blue: 90/255.0, alpha: 1.0)
    static let flushWhite = UIColor(red: 241/255.0, green: 238/255.0, blue: 231/255.0, alpha: 1.0)
    static let lightGray = UIColor(displayP3Red: 162/255.0, green: 163/255.0, blue: 159/255.0, alpha: 1.0)
    static let darkGray = UIColor(red: 80/255.0, green: 84/255.0, blue: 85/255.0, alpha: 1.0)
    static let royalGreen = UIColor(red: 31/255.0, green: 46/255.0, blue: 47/255.0, alpha: 1.0)
    static let lightRoyalGreen = UIColor(red: 72/255.0, green: 107/255.0, blue: 109/255.0, alpha: 1.0)
}
