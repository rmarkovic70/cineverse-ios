//
//  Keyes.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

struct Keyes {
    static let ApiKey = "5945b977d09147cc3f80548a2b019939"
    static let ApiReadAccessToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1OTQ1Yjk3N2QwOTE0N2NjM2Y4MDU0OGEyYjAxOTkzOSIsInN1YiI6IjYyY2U5ZGY3NjMzMWIyMDBmMzI4YzhmNSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.TCNi8HaG1nyYqJFG9SEMvLSJgelAfMd6ySpQAZl2Idw"
}

struct URLs {
    static let apiURL = "https://api.themoviedb.org/3/movie/now_playing?api_key=\(Keyes.ApiKey)&page="
}

