//
//  Models.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit

struct Result: Codable {
    let results: [Movie]?
}

struct Movie: Codable {
    let adult: Bool?
    let genre_ids: [Int]?
    let id: Int?
    let original_title, overview: String?
    let popularity: Double?
    let poster_path: String?
    let release_date, title: String?
    let video: Bool?
    let vote_average: Double?
    let vote_count: Int?
}

struct Dates {
    let maximum, minimum: String
}
