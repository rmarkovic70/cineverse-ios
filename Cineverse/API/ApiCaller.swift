//
//  ApiCaller.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit
import Combine

enum FetchPostsError: Error {
    case custom(error: Error)
    case wrongUrl
    case unknown
    case responseNotOk
    
    var errorDescription: String? {
        switch self {
        case .custom:
            return "Sth went wrong."
        case .unknown:
            return "Unknown decode error."
        case .wrongUrl:
            return "Wrong URL"
        case .responseNotOk:
            return "HTTP Response not 2xx."
        }
    }
}

class ApiCaller {
    
    static let shared = ApiCaller()
    
    func getMovieList(at page: Int) -> AnyPublisher<[Movie], Error> {
        guard let url = URL(string: URLs.apiURL + "\(page)") else {
            return Fail(outputType: [Movie].self, failure: FetchPostsError.wrongUrl).eraseToAnyPublisher()
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Keyes.ApiReadAccessToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "accept")
            
        return URLSession.shared.dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .tryMap { result in
                guard let httpResponse = result.response as? HTTPURLResponse,
                        httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 else {
                    throw FetchPostsError.responseNotOk
                }
                    
                let decoder = JSONDecoder()
                guard let response = try? decoder.decode(Result.self, from: result.data) else {
                    throw FetchPostsError.unknown
                }
                guard let movies = response.results else {
                    throw FetchPostsError.unknown
                }
                return movies
            }
            .eraseToAnyPublisher()
    }
}
