//
//  MovieFilter1.swift
//  Cineverse
//
//  Created by Apis IT on 28.08.2024..
//

import UIKit

protocol Filtering {
    func filterMoviesByGenre(unfilteredMovies: [Movie], userPrefferedGenres: [Int]) -> [Movie]
}

class MovieFilter1: Filtering {
    
    static let shared = MovieFilter1()
    
    internal func filterMoviesByGenre(unfilteredMovies: [Movie], userPrefferedGenres: [Int]) -> [Movie] {
        var filteredMoviesByGenre: [Movie] = []
        
        if !userPrefferedGenres.isEmpty {
            for movie in unfilteredMovies {
                guard let singleMovieGenre = movie.genre_ids?.first else {break}
                if userPrefferedGenres.contains(singleMovieGenre) {
                    filteredMoviesByGenre.append(movie)
                }
            }
            return filteredMoviesByGenre
        }
        else {
            filteredMoviesByGenre = unfilteredMovies
            return filteredMoviesByGenre
        }
    }
}
