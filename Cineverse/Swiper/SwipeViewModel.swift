//
//  SwipeViewModel.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit
import Combine

enum State {
    case empty
    case loading
    case loaded
    case filter
}

class SwipeViewModel {
    
    @Published var unfilteredMovies: [Movie]
    @Published var state: State
    private var bag: Set<AnyCancellable>
    var filteredMoviesByGenre: [Movie]
    var userPrefferedGenres: [Int]
    var page: Int
    var movieFilter1: Filtering
    
    init(movies: [Movie]) {
        self.unfilteredMovies = []
        self.filteredMoviesByGenre = []
        self.userPrefferedGenres = []
        self.state = .empty
        self.bag = Set<AnyCancellable>()
        self.page = 1
        self.movieFilter1 = MovieFilter1()
    }
    
    func getApiMovies() {
        ApiCaller.shared.getMovieList(at: page)
            .sink(receiveCompletion: { _ in
            }, receiveValue: { [weak self] apiMovies in
                self?.unfilteredMovies = apiMovies
                self?.state = .filter
            })
            .store(in: &bag)
    }

    func saveUserPreferencedMovies(movie: Movie) {
        if let genreId = movie.genre_ids?.first {
            if !userPrefferedGenres.contains(genreId) {
                userPrefferedGenres.insert(genreId, at: 0)
            }
        }
        if userPrefferedGenres.count > 3 {
            userPrefferedGenres.removeLast()
        }
    }
    
    func filterMovies() {
        filteredMoviesByGenre = MovieFilter1.shared.filterMoviesByGenre(unfilteredMovies: unfilteredMovies, userPrefferedGenres: userPrefferedGenres)
        state = .loading
    }
}
