//
//  SwipeViewController.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit
import Combine

class SwipeViewController: UIViewController {

    let swipeViewModel: SwipeViewModel
    private var bag: Set<AnyCancellable>
    
    init(swipeViewModel: SwipeViewModel) {
        self.swipeViewModel = swipeViewModel
        self.bag = Set<AnyCancellable>()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var appTitle: UILabel = {
        let label = UILabel()
        label.text = "Cineverse"
        label.font = UIFont(name: "BebasNeue-Regular", size: 35)
        label.textColor = Colors.matteGold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewUI()
        bind()
        addSubviews()
    }
    
    func setupViewUI() {
        view.backgroundColor = Colors.royalGreen
    }
    
    func bind() {
        swipeViewModel.state = .empty
        
        swipeViewModel.$unfilteredMovies
            .combineLatest(swipeViewModel.$state)
            .receive(on: RunLoop.main)
            .sink { [weak self] movies in
                self?.reloadState()
            }
            .store(in: &bag)
    }
    
    func reloadState() {
        switch swipeViewModel.state {
        case .empty:
            swipeViewModel.getApiMovies()
        case .filter:
            swipeViewModel.filterMovies()
        case .loading:
            generateCards()
        case .loaded:
            swipeViewModel.filteredMoviesByGenre.removeAll()
        }
    }

    func generateCards() {
        for movie in swipeViewModel.filteredMoviesByGenre {
            let movieCard = MovieCard(movie: movie)
            movieCard.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:))))
            addMovieCardAsSubview(movieCard)
        }
        swipeViewModel.state = .loaded
    }
    
    func addMovieCardAsSubview(_ movieCard: MovieCard) {
        if let cardAbove = view.subviews.first { /// The order of the subviews manages order on the screen, subview at index 0 being the back-most view.
            view.insertSubview(movieCard, belowSubview: cardAbove)
        }
        constraintMovieCard(movieCard)
    }
    
    func checkNumOfMovieCardsLeft() {
        let movieCardsStacked = self.view.subviews.count - 2 /// Minus 1 because of app title subview
        if movieCardsStacked < 5 {
            swipeViewModel.page += 1
            swipeViewModel.state = .empty
        }
    }
    
    @objc func panGesture(_ sender: UIPanGestureRecognizer) {
        handlePanGesture(sender)
    }
    
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        guard let movieCardView = sender.view as? MovieCard else {return}
        let point = sender.translation(in: view)
        movieCardView.center = CGPoint(x: view.center.x + point.x, y: view.center.y + point.y)
        let xFromCenter = movieCardView.center.x - view.center.x
               
        let divisor = (view.frame.width / 2) / 0.61
        let scale = min(100 / abs(xFromCenter), 1)
        
        if xFromCenter > 0 {
            movieCardView.setupThumbDown()
        } else if xFromCenter < 0 {
            movieCardView.setupThumbUp()
        }
        movieCardView.thumb.alpha = abs(xFromCenter) / view.center.x
        movieCardView.transform = CGAffineTransform(rotationAngle: xFromCenter / divisor).scaledBy(x: scale, y: scale)
           
        if sender.state == .ended {
            if movieCardView.center.x < 75 {
                UIView.animate(withDuration: 0.3) {
                    movieCardView.center = CGPoint(x: self.view.center.x - 200, y: self.view.center.y + 75)
                    movieCardView.alpha = 0
                }
                checkNumOfMovieCardsLeft()
                removeCardFromSuperview(movieCardView)
            } else if movieCardView.center.x > (view.frame.width - 75) {
                UIView.animate(withDuration: 0.3) {
                    movieCardView.center = CGPoint(x: self.view.center.x + 200, y: self.view.center.y + 75)
                    movieCardView.alpha = 0
                }
                swipeViewModel.saveUserPreferencedMovies(movie: movieCardView.movie)
                checkNumOfMovieCardsLeft()
                removeCardFromSuperview(movieCardView)
            } else {
                UIView.animate(withDuration: 0.3) {
                    movieCardView.thumb.alpha = 0
                    movieCardView.center = self.view.center
                    movieCardView.transform = .identity
                }
            }
        }
    }

    func constraintMovieCard(_ movieCard: MovieCard) {
        movieCard.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        movieCard.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        movieCard.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
        movieCard.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.65).isActive = true
//        movieCard.topAnchor.constraint(equalTo: appTitle.bottomAnchor, constant: 20).isActive = true
//        movieCard.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        movieCard.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        movieCard.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
    }
    
    func addSubviews() {
        view.addSubview(appTitle)
        constraint()
    }
    
    func constraint() {
        appTitle.topAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: 5).isActive = true
        appTitle.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        appTitle.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        appTitle.heightAnchor.constraint(lessThanOrEqualToConstant: 50).isActive = true
    }
    
    func removeCardFromSuperview(_ card: UIView) {
        card.removeFromSuperview()
    }
}
