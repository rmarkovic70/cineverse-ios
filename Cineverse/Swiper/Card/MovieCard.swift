import UIKit
import Nuke

enum CardStatus {
    case accepted
    case rejected
    case undetermined
}

enum MovieGenre: Int {
    case action = 28
    case adventure = 12
    case animation = 16
    case comedy = 35
    case crime = 80
    case documentary = 99
    case drama = 18
    case family = 10751
    case fantasy = 14
    case history = 36
    case horror = 27
    case music = 10402
    case mystery = 9648
    case romance = 10749
    case scienceFiction = 878
    case tvMovie = 10770
    case thriller = 53
    case war = 10752
    case western = 37
    
    var name: String {
        switch self {
        case .action: return "Action"
        case .adventure: return "Adventure"
        case .animation: return "Animation"
        case .comedy: return "Comedy"
        case .crime: return "Crime"
        case .documentary: return "Documentary"
        case .drama: return "Drama"
        case .family: return "Family"
        case .fantasy: return "Fantasy"
        case .history: return "History"
        case .horror: return "Horror"
        case .music: return "Music"
        case .mystery: return "Mystery"
        case .romance: return "Romance"
        case .scienceFiction: return "Science Fiction"
        case .tvMovie: return "TV Movie"
        case .thriller: return "Thriller"
        case .war: return "War"
        case .western: return "Western"
        }
    }
    
    init?(from rawValue: Int) {
            self.init(rawValue: rawValue)
        }
}

class MovieCard: UIView {
    
    var cardStatus: CardStatus
    var movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
        self.cardStatus = .undetermined
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        setupUI(movie)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var container: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.lightRoyalGreen
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var thumb: UIImageView = {
        let image = UIImageView()
        image.alpha = 0
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var poster: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 15
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var genre: UILabel = {
        let label = UILabel()
        label.textColor = Colors.flushWhite
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var releaseYear: UILabel = {
        let label = UILabel()
        label.textColor = Colors.flushWhite
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var overview: UILabel = {
        let label = UILabel()
        label.textColor = Colors.flushWhite
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var addToFavoritesButton: UIButton = {
        let button = UIButton()
        let iconConfig = UIImage.SymbolConfiguration(pointSize: 15, weight: .thin, scale: .large)
        let icon = UIImage(systemName: "star", withConfiguration: iconConfig)
        let iconFill = UIImage(systemName: "star.fill", withConfiguration: iconConfig)
        button.setImage(icon, for: .normal)
        button.setImage(iconFill, for: .highlighted)
        button.tintColor = .blue
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    func setupUI(_ movie: Movie) {
        self.setupPosterImage(url: ("https://image.tmdb.org/t/p/original" + (movie.poster_path ?? "")))
        self.genre.text = "Genre: \(prepareGenres(movie.genre_ids ?? []))"
        self.overview.text = "Summary: \(movie.overview ?? "Unavailable")"
        if let date = DateFormatterManager.shared.format(jsonDate: movie.release_date ?? "Unavailable") {
            self.releaseYear.text = "Released: \(date)"
        }
        addSubviews()
    }
    
    func prepareGenres(_ genreIDs: [Int]) -> String {
        if genreIDs.isEmpty {
            return "Unavailable"
        }
        let stringGenres = genreIDs.compactMap { genreID -> String? in
            if let genre = MovieGenre(rawValue: genreID) {
                return genre.name
            } else {
                return nil
            }
        }
        return stringGenres.joined(separator: ", ")
    }

    func setupPosterImage(url: String) {
        guard let url = URL(string: url) else { return }

        ImagePipeline.shared.loadImage(with: url) { result in
                    switch result {
                    case .success(let response):
                        DispatchQueue.main.async {
                            self.poster.image = response.image
                        }
                    case .failure(let error):
                        print("Failed to load image: \(error)")
                    }
                }
    }

    
    func setupThumbDown() {
        thumb.image = UIImage(systemName: "hand.thumbsup.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 100, weight: .thin, scale: .large))
        thumb.tintColor = .green
    }
    
    func setupThumbUp() {
        thumb.image = UIImage(systemName: "hand.thumbsdown.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 100, weight: .thin, scale: .large))
        thumb.tintColor = .red
    }
    
    func addSubviews() {
        container.addSubview(poster)
        container.addSubview(genre)
        container.addSubview(releaseYear)
        container.addSubview(overview)
        container.addSubview(thumb)
        self.addSubview(container)
        constraint()
    }
    
    func constraint() {
        poster.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        poster.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        poster.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        poster.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -150).isActive = true
        
        genre.topAnchor.constraint(equalTo: poster.bottomAnchor, constant: 15).isActive = true
        genre.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 15).isActive = true
        genre.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -15).isActive = true
        genre.bottomAnchor.constraint(equalTo: releaseYear.topAnchor, constant: 0).isActive = true
        
        releaseYear.topAnchor.constraint(equalTo: genre.bottomAnchor, constant: 10).isActive = true
        releaseYear.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 15).isActive = true
        releaseYear.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -15).isActive = true
        releaseYear.bottomAnchor.constraint(equalTo: overview.topAnchor, constant: 0).isActive = true
        
        overview.topAnchor.constraint(equalTo: releaseYear.bottomAnchor, constant: 10).isActive = true
        overview.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 15).isActive = true
        overview.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -15).isActive = true
        overview.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -15).isActive = true
        
        thumb.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        thumb.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        
        container.topAnchor.constraint(equalTo: topAnchor).isActive = true
        container.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        container.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
