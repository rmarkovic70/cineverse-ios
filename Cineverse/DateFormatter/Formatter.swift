//
//  Formatter.swift
//  Cineverse
//
//  Created by Apis IT on 04.09.2024..
//

import Foundation

protocol Date {
    
}

class DateFormatterManager {
    
    static let shared = DateFormatterManager()
    
    private let inputFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
    }()
    
    private let outputFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
        
    func format(jsonDate: String) -> String? {
        if let date = inputFormatter.date(from: jsonDate) {
            return outputFormatter.string(from: date)
        } else {
            return nil
        }
    }
}
