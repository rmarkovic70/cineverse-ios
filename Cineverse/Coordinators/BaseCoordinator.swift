//
//  BaseCoordinator.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit

class BaseCoordinator: NSObject {
    
    override init() {}
    
    func start() {}
}
