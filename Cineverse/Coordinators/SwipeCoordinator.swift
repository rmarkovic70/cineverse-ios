//
//  SwipeCoordinator.swift
//  Cineverse
//
//  Created by Apis IT on 09.03.2024..
//

import UIKit

class SwipeCoordinator: BaseCoordinator {
    
    let window: UIWindow
    let navigationController: UINavigationController
    
    init(window: UIWindow) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    override func start() {
        let swipeVC = SwipeViewController(swipeViewModel: SwipeViewModel(movies: []))
        navigationController.pushViewController(swipeVC, animated: false)
        window.rootViewController = navigationController
    }
}
